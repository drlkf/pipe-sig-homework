#include		<stdlib.h>
#include		<stdio.h>
#include		"resources.h"
#include		"parent.h"
#include		"child.h"

t_opt			optfuns[] = {
	{'n', opt_n},
	{'t', opt_t},
	{'l', opt_l},
	{0, NULL}
};

static int	set_opt(int opt, const char *optarg, t_args *args)
{
	int		i;

	i = 0;
	while (optfuns[i].c != 0)
	{
		if (optfuns[i].c == opt)
			return (optfuns[i].fun(args, optarg));
		i++;
	}

	return (1);
}

static void	program_start(t_args *args)
{
	if (is_child(args))
		child_exec(args);
	else
		parent_exec(args);
}

int			main(int argc, char *argv[])
{
	int		opt;
	t_args	args;

	args.iter = ITER_DEFAULT;
	args.type = E_NONE;
	args.log = 0;
	args.total_sent = 0;
	args.total_recv = 0;
	opt = getopt(argc, argv, "n:t:l");
	while (opt != -1)
	{
		if (opt == '?')
			return (1);
		if (set_opt(opt, optarg, &args))
		{
			fprintf(stderr, "%s: invalid argument for option '%c'\n",
					argv[0], opt);
			return (1);
		}
		opt = getopt(argc, argv, "n:t:l");
	}
	if (args.type == E_NONE)
	{
		fprintf(stderr, "%s: option 't' is required\n", argv[0]);
		return (1);
	}
	if (init_resources(&args))
	{
		fprintf(stderr, "%s: could not initialize necessary system resources\n", argv[0]);
		error_handle(&args);
		clear_resources(&args);
		return (1);
	}
	program_start(&args);
	clear_resources(&args);

	return (0);
}
