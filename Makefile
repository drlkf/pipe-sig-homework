NAME		=	homework-linux

RM		=	rm -rf
MKDIR		=	mkdir -p

CC		=	gcc
CFLAGS		=	-Wall -Wextra -Werror
CFLAGS		+=	-O3
IFLAGS		=	-I include
LFLAGS		=

SRCDIR		=	src
SRCFILES	=	main.c		\
			parent.c	\
			child.c		\
			args.c		\
			resources.c	\
			loop.c		\
			sigs.c		\
			err_log.c	\
			print.c		\

SRC		=	$(SRCFILES:%.c=$(SRCDIR)/%.c)

OBJDIR		=	.obj
OBJFILES	=	$(SRCFILES:%.c=%.o)
OBJ		=	$(OBJFILES:%=$(OBJDIR)/%)

all:		$(NAME)

$(NAME):	$(OBJ)
		$(CC) $(CFLAGS) $(LFLAGS) -o $@ $(OBJ)

$(OBJ):		| $(OBJDIR)

$(OBJDIR):
		$(MKDIR) $@

$(OBJDIR)/%.o:	$(SRCDIR)/%.c
		$(CC) $(CFLAGS) $(IFLAGS) -c $< -o $@

clean:
		$(RM) $(OBJDIR)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		clean fclean all re
