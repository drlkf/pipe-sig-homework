
#ifndef RESOURCES_H_
# define RESOURCES_H_

# include	"args.h"

int	init_resources(t_args *args);
void	error_handle(t_args *args);
void	clear_resources(t_args *args);

#endif /* !RESOURCES_H_ */
