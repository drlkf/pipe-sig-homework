#include	<stdio.h>
#include	"child.h"
#include	"loop.h"
#include	"err_log.h"

static int	send_confirm(t_args *args)
{
	char		buf;
	int		ret;

	buf = 0;
	ret = write(args->fd[E_CHWR][E_WR], &buf, sizeof(buf));
	if (ret != sizeof(buf))
	{
		perror("write");
		return (1);
	}
	args->total_sent += ret;

	return (0);
}

static int	send_data(const t_args *args)
{
	int		tmp;
	int		written;
	int		to_write;

	written = 0;
	to_write = ((args->iter + 1) * sizeof(*(args->results)));
	while (written < to_write)
	{
		tmp = write(args->fd[E_CHWR][E_WR],
				(args->results + written), (to_write - written));
		if (tmp == -1)
		{
			perror("write");
			return (1);
		}
		written += tmp;
		((t_args *) args)->total_sent += tmp;
	}
	return (0);
}

void			child_exec(t_args *args)
{
	int		i;
	int		max;

	if (send_confirm(args))
	{
		fprintf(stderr,
				"child error: could not send confirm byte to parent\n");
		return;
	}

	i = 0;
	max = args->iter;
	loop_init(args);
	handle_byte(args, i);
	while (i < max)
	{
		if (recv_byte(args))
		{
			fprintf(stderr,
					"child process: error occured while receiving byte\n");
			return;
		}
		if (send_byte(args))
		{
			fprintf(stderr,
					"child process: error occured while sending byte\n");
			return;
		}
		handle_byte(args, (i + 1));
		i++;
	}

	if (send_data(args))
		fprintf(stderr, "child process: error occured while sending results\n");
}
