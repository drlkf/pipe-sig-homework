#include		<stdio.h>
#include		<stdlib.h>
#include		<sys/types.h>
#include		<signal.h>
#include		"sigs.h"
#include		"loop.h"
#include		"err_log.h"

static int	(*send_fun)(t_args *);
static int	(*recv_fun)(t_args *);

static int	send_sig(t_args *args)
{
	if (is_child(args))
		err_log(args, "child preparing to send sig\n");
	else
		err_log(args, "parent preparing to send sig\n");
	if (kill(args->pid[args->to_kill], SIGUSR1) == -1)
	{
		perror("kill");
		return (1);
	}

	if (is_child(args))
		err_log(args, "child sent sig\n");
	else
		err_log(args, "parent sent sig\n");
	return (0);
}

static int	send_pipe(t_args *args)
{
	int		ret;
	int		pipe_type;
	char		byte;

	if (is_child(args))
	{
		byte = 1;
		pipe_type = E_CHWR;
	}
	else
	{
		byte = 0;
		pipe_type = E_PRWR;
	}
	ret = write(args->fd[pipe_type][E_WR], &byte, sizeof(byte));
	if (ret == -1)
	{
		perror("write");
		return (1);
	}
	args->total_sent += ret;
	return (0);
}

static int	recv_sig(t_args *args)
{
	sigset_t	mask;

	if (is_child(args))
		err_log(args, "child receiving sig...\n");
	else
		err_log(args, "parent receiving sig...\n");
	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR1);
	sigsuspend(&(args->sigmask));
	sigprocmask(SIG_UNBLOCK, &mask, NULL);
	if (is_child(args))
		err_log(args, "child received sig...\n");
	else
		err_log(args, "parent received sig...\n");
	if (is_child(args))
		err_log(args, "child preparing to receive sig\n");
	else
		err_log(args, "parent preparing to receive sig\n");
	if (handle_sigusr1())
	{
		fprintf(stderr, "error while handling SIGUSR1\n");
		return (1);
	}
	if (is_child(args))
		err_log(args, "child ready to receive sig\n");
	else
		err_log(args, "parent ready to receive sig\n");
	return (0);
}

static int	recv_pipe(t_args *args)
{
	int		ret;
	int		pipe_type;
	char		byte;

	if (is_child(args))
	{
		byte = 1;
		pipe_type = E_CHRD;
	}
	else
	{
		byte = 0;
		pipe_type = E_PRRD;
	}
	byte = 42;
	ret = read(args->fd[pipe_type][E_RD], &byte, sizeof(byte));
	if (ret == -1)
	{
		perror("read");
		return (1);
	}
	args->total_recv += ret;

	return (0);
}

t_send_fun	send_funs[] = {
	{E_SIG, send_sig},
	{E_PIPE, send_pipe},
	{E_NONE, NULL}
};

t_recv_fun	recv_funs[] = {
	{E_SIG, recv_sig},
	{E_PIPE, recv_pipe},
	{E_NONE, NULL}
};

int			send_byte(t_args *args)
{
	return ((*send_fun)(args));
}

int			recv_byte(t_args *args)
{
	return ((*recv_fun)(args));
}

int			loop_init(t_args *args)
{
	char			type;
	int			is_set[2];
	int			i;

	is_set[E_RD] = 1;
	is_set[E_WR] = 1;
	type = args->type;
	i = 0;
	while (recv_funs[i].type != E_NONE)
	{
		if (recv_funs[i].type == type)
		{
			recv_fun = recv_funs[i].fun;
			is_set[E_RD] = 0;
			break;
		}
		i++;
	}

	i = 0;
	while (send_funs[i].type != E_NONE)
	{
		if (send_funs[i].type == type)
		{
			send_fun = send_funs[i].fun;
			is_set[E_WR] = 0;
			break;
		}
		i++;
	}

	gettimeofday(args->results, NULL);

	return (is_set[E_RD] || is_set[E_WR]);
}

void	handle_byte(t_args *args, int i)
{
	gettimeofday(&(args->results[i]), NULL);
}
