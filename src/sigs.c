#include	<stdio.h>
#include	<stdlib.h>
#include	<signal.h>
#include	"sigs.h"
#include	"resources.h"
#include	"parent.h"
#include	"child.h"
#include	"err_log.h"

static t_args	*g_args;

void			sigs_init(t_args *args)
{
	g_args = args;
}

static void	interrupt_handler(int arg)
{
	fprintf(stderr, "signal (%d) received, exiting...\n", arg);
	clear_resources(g_args);
	exit(arg);
}

static void	recv_handler(int arg)
{
	(void) arg;
	if (is_child(g_args))
		err_log(g_args, "child received sigusr1...\n");
	else
		err_log(g_args, "parent received sigusr1...\n");
}

int			handle_sigusr1()
{
	sigset_t	mask;

	if (is_child(g_args))
		err_log(g_args, "child handling sigusr1...\n");
	else
		err_log(g_args, "parent handling sigusr1...\n");
	sigemptyset(&mask);
	sigaddset(&mask, SIGUSR1);
	if (sigprocmask(SIG_BLOCK, &mask, &(g_args->sigmask)) == -1 ||
			signal(SIGUSR1, recv_handler) == SIG_ERR)
	{
		perror("signal");
		return (1);
	}
	if (is_child(g_args))
		err_log(g_args, "child ok\n");
	else
		err_log(g_args, "parent ok\n");

	return (0);
}

static int	handle_interrupt(int sig)
{
	if (signal(sig, interrupt_handler) == SIG_ERR)
	{
		perror("signal");
		return (1);
	}

	return (0);
}

int			handle_sigint()
{
	return (handle_interrupt(SIGINT));
}

int			handle_sighup()
{
	return (handle_interrupt(SIGHUP));
}
