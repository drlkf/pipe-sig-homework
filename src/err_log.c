#include		<stdio.h>
#include		<string.h>
#include		"err_log.h"

void			err_log(t_args *args, const char *fmt)
{
	if (args->log)
		write(args->log_fd, fmt, strlen(fmt));
}
