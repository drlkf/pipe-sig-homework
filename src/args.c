#include	<stdlib.h>
#include	<strings.h>
#include	"args.h"

int		opt_n(t_args *args, const char *optarg)
{
	args->iter = atoi(optarg);
	return (args->iter <= 0);
}

int		opt_l(t_args *args, const char *optarg)
{
	(void) optarg;
	args->log = 1;
	return (0);
}

int		opt_t(t_args *args, const char *optarg)
{
	if (!strcasecmp("signal", optarg) ||
			!strcasecmp("sig", optarg) ||
			!strcasecmp("s", optarg))
		args->type = E_SIG;
	else if (!strcasecmp("pipe", optarg) ||
			!strcasecmp("p", optarg))
		args->type = E_PIPE;
	else
		return (1);

	return (0);
}

int		is_child(const t_args *args)
{
	return (args->is_child);
}

int		child_exists(const t_args *args)
{
	return (args->child_exists);
}
