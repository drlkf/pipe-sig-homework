
#ifndef	ARGS_H_
# define	ARGS_H_

# include	<signal.h>
# include	<unistd.h>
# include	<sys/time.h>

# define E_NONE			(-1)
# define E_SIG				(0)
# define E_PIPE			(1)

# define E_ERR				(-1)
# define E_PARENT			(0)
# define E_CHILD			(1)

# define E_RD				(0)
# define E_WR				(1)

# define E_CHRD			(0)
# define E_CHWR			(1)
# define E_PRRD			(1)
# define E_PRWR			(0)

# define	FD_N				(2)

# define ITER_DEFAULT	(100)
# define LOG_FILEPATH	("./test.log")

typedef struct timeval	t_timer;

typedef struct	s_args
{
	char			type;
	int			iter;
	int			fd[2][2];
	int			log_fd;
	int			child_exists;
	int			is_child;
	int			to_kill;
	pid_t			pid[2];
	sigset_t		sigmask;
	t_timer		*results;
	int			total_sent;
	int			total_recv;
	int			log;
}					t_args;

typedef struct	s_opt
{
	char			c;
	int			(*fun)(t_args *, const char *);
}					t_opt;

int				opt_n(t_args *args, const char *optarg);
int				opt_t(t_args *args, const char *optarg);
int				opt_l(t_args *args, const char *optarg);

int				is_child(const t_args *args);
int				child_exists(const t_args *args);

#endif /* !ARGS_H_ */
