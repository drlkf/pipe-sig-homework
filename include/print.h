
#ifndef PRINT_H_
# define PRINT_H_

# include	"args.h"

# define		E_MIN	(0)
# define		E_MAX	(1)
# define		E_AVG	(2)

void	print_infos(const t_timer *results, int n);

#endif /* !PRINT_H_ */
