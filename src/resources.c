#include		<stdio.h>
#include		<stdlib.h>
#include		<unistd.h>
#include		<fcntl.h>
#include		<sys/stat.h>
#include		<sys/types.h>
#include		<sys/wait.h>
#include		"resources.h"
#include		"sigs.h"
#include		"loop.h"

static void	close_fd(int fd)
{
	if (fd == -1)
		return;
	else if (close(fd) == -1)
		perror("close");
}

void			clear_resources(t_args *args)
{
	pid_t		ret;
	int		status;

	free(args->results);
	if (is_child(args))
	{
		close_fd(args->fd[E_CHRD][E_RD]);
		close_fd(args->fd[E_CHWR][E_WR]);
	}
	else
	{
		close_fd(args->fd[E_PRRD][E_RD]);
		close_fd(args->fd[E_PRWR][E_WR]);
	}
	if (args->log)
		close_fd(args->log_fd);
	if (child_exists(args) && !is_child(args))
	{
		ret = wait(&status);
		if (ret == -1)
			perror("wait");
	}
}

int			init_resources(t_args *args)
{
	pid_t		pid;

	args->fd[E_PRRD][E_RD] = -1;
	args->fd[E_PRRD][E_WR] = -1;
	args->fd[E_PRWR][E_RD] = -1;
	args->fd[E_PRWR][E_WR] = -1;
	args->pid[E_CHILD] = -1;
	args->is_child = 0;
	args->child_exists = 0;
	args->results = NULL;

	if (args->log)
	{
		args->log_fd = open(LOG_FILEPATH, O_CREAT|O_TRUNC|O_WRONLY, S_IRUSR|S_IWUSR);
		if (args->log_fd == -1)
		{
			perror("open");
			fprintf(stderr, "error: could not open log file\n");
			return (1);
		}
	}

	if (pipe(args->fd[E_CHRD]) == -1 ||
			pipe(args->fd[E_CHWR]))
	{
		perror("pipe");
		return (1);
	}

	loop_init(args);
	args->pid[E_PARENT] = getpid();

	pid = fork();
	if (pid == -1)
	{
		perror("fork");
		return (1);
	}

	args->results = malloc((args->iter + 1) * sizeof(*(args->results)));
	if (!args->results)
	{
		perror("malloc");
		return (1);
	}

	if (pid == 0)
	{
		args->child_exists = 1;
		args->is_child = 1;
		args->to_kill = E_PARENT;
		if (close(args->fd[E_CHRD][E_WR]) == -1 ||
				close(args->fd[E_CHWR][E_RD]))
		{
			perror("close");
			return (1);
		}
	}
	else
	{
		args->child_exists = 1;
		args->pid[E_CHILD] = pid;
		args->to_kill = E_CHILD;
		if (close(args->fd[E_PRWR][E_RD]) == -1 ||
				close(args->fd[E_PRRD][E_WR]))
		{
			perror("close");
			return (1);
		}
	}

	sigs_init(args);
	if (handle_sigint() || handle_sighup() ||
			(args->type == E_SIG && handle_sigusr1()))
	{
		fprintf(stderr, "error: could not init signal handlers\n");
		return (1);
	}

	return (0);
}

void			error_handle(t_args *args)
{
	int		ret;
	char		buf;

	buf = 1;
	if (is_child(args))
	{
		ret = write(args->fd[E_CHWR][E_WR], &buf, sizeof(buf));
		if (ret != sizeof(buf))
		{
			perror("write");
			fprintf(stderr,
					"child warning: could not send error code to parent\n");
		}
		args->total_sent += ret;
	}
}
