
#ifndef LOOP_H_
# define LOOP_H_

# include	"args.h"

typedef struct	s_send_fun
{
	char			type;
	int			(*fun)(t_args *);
}					t_send_fun;

typedef struct	s_recv_fun
{
	char			type;
	int			(*fun)(t_args *);
}					t_recv_fun;

int	send_byte(t_args *args);
int	recv_byte(t_args *args);
void	handle_byte(t_args *args, int i);
int	loop_init(t_args *args);

#endif /* !LOOP_H_ */
