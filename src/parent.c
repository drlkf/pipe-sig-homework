#include		<stdlib.h>
#include		<stdio.h>
#include		<sys/wait.h>
#include		"print.h"
#include		"parent.h"
#include		"loop.h"

static int	get_infos_from_child(const t_args *args, t_timer *buf)
{
	int		tmp;
	int		bytes_read;
	int		to_read;

	bytes_read = 0;
	to_read = ((args->iter + 1) * sizeof(*buf));
	while (bytes_read < to_read)
	{
		tmp = read(args->fd[E_PRRD][E_RD], (buf + bytes_read), (to_read - bytes_read));
		if (tmp == -1)
		{
			perror("read");
			return (1);
		}
		bytes_read += tmp;
		((t_args *) args)->total_recv += tmp;
	}
	return (0);
}

static void	print_results(const t_args *args,
		const t_timer *child_results)
{
	gid_t		gid;

	gid = getgid();
	printf("process id is %d, group id is %d\n",
			args->pid[E_PARENT], gid);
	print_infos(args->results, args->iter);
	printf("child process id is %d, group id is %d\n",
			args->pid[E_CHILD], gid);
	print_infos(child_results, args->iter);
}

static char	receive_confirm(t_args *args)
{
	int		ret;
	char		byte;

	byte = 0;
	ret = read(args->fd[E_PRRD][E_RD], &byte, sizeof(byte));
	if (ret == -1)
	{
		perror("read");
		return (1);
	}

	return (byte);
}

static int	wait_for_child(t_args *args)
{
	int		ret;
	int		status;

	ret = wait(&status);
	if (ret == -1)
		perror("wait");
	args->child_exists = 0;
	return (ret == -1);
}

void			parent_exec(t_args *args)
{
	int		i;
	int		max;
	t_timer	*child_results;

	if (receive_confirm(args))
	{
		fprintf(stderr, "parent error: did not receive confirm from child\n");
		return;
	}
	i = 0;
	max = args->iter;
	loop_init(args);
	while (i < max)
	{
		if (send_byte(args))
		{
			fprintf(stderr,
					"parent process: error occured while sending byte\n");
			return;
		}
		if (recv_byte(args))
		{
			fprintf(stderr,
					"parent process: error occured while receiving byte\n");
			return;
		}
		handle_byte(args, (i + 1));
		i++;
	}

	child_results = malloc((args->iter + 1) * sizeof(*child_results));
	if (!child_results)
	{
		perror("malloc");
		fprintf(stderr,
				"parent error: could not allocate memory\n");
		return;
	}
	if (get_infos_from_child(args, child_results))
	{
		fprintf(stderr,
				"parent process: error occured while retrieving data from child\n");
	}
	if (wait_for_child(args))
		fprintf(stderr,
				"parent warning: error occured while waiting for child\n");
	print_results(args, child_results);
	free(child_results);
}
