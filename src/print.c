#include	<stdio.h>
#include	"print.h"

void			get_values_from_results(const t_timer *results,
		int n, double values[3])
{
	int		i;
	double	current;

	i = 0;
	current = (results[i + 1].tv_sec - results[i].tv_sec) +
		((results[i + 1].tv_usec - results[i].tv_usec) / 1000000.0);
	values[E_MIN] = current;
	values[E_MAX] = current;
	values[E_AVG] = 0.0;
	while (i < n)
	{
		current = (results[i + 1].tv_sec - results[i].tv_sec) +
			((results[i + 1].tv_usec - results[i].tv_usec) / 1000000.0);
		if (values[E_MIN] > current)
			values[E_MIN] = current;
		if (values[E_MAX] < current)
			values[E_MAX] = current;
		values[E_AVG] += current;
		i++;
	}
	values[E_AVG] /= n;
}

void			print_infos(const t_timer *results, int n)
{
	double	values[3];

	get_values_from_results(results, n, values);
	printf("\tAverage time: %fs\n", values[E_AVG]);
	printf("\tMinimum time: %fs\n", values[E_MIN]);
	printf("\tMaximum time: %fs\n", values[E_MAX]);
}

