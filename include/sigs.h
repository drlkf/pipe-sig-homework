
#ifndef SIGS_H_
# define SIGS_H_

# include	"args.h"

void	sigs_init(t_args *args);
int	handle_sigusr1();
int	handle_sigint();
int	handle_sighup();

#endif /* !SIGS_H_ */
